from socketIO_client_nexus import SocketIO, LoggingNamespace
from libs.deteccion import deteccion as det
from libs.centroidtracker import CentroidTracker
from libs.trackableobject import TrackableObject
from libs.sendEmail import sendEmail
from copy import deepcopy
from os import environ as env
import tensorflow as tf
import numpy as np
import utilidades
import threading
# import argparse
import requests
import base64
import time
import cv2
import dlib

flag_gT = 0
time_gt = 0


def getFrame(*args):
    global trama
    global flag_gT
    global time_gt
    global flag_pcss
    threadLock.acquire()
    trama = deepcopy(args)
    threadLock.release()
    getF.acquire()
    flag_pcss = True
    getF.release()
    flag_gT += 1
    print("recibi frame:", flag_gT, 'len(trama):', len(trama))
    temp = time.time()
    # print('tiempo recibi frame:', (temp-time_gt))
    time_gt = temp


def escuchar():
    global socketIO

    while(1):
        socketIO.on('frame', getFrame)
        socketIO.wait(seconds=0.001)


def sendEm():
    global socketIO
    global frameS
    global dataS
    global timeF
    global emailF
    global cond
    global stateMail
    global whoMail
    global ips
    global port
    global totalUp2
    global totalDown2
    global totalIngreso2
    global totalSalida2
    global http_s
    while(1):
        if stateMail is True:
            lockData.acquire()
            frameSL = deepcopy(frameS)
            dataSL = deepcopy(dataS)
            In = deepcopy(totalUp2)
            Out = deepcopy(totalDown2)
            lockData.release()

            if cond == "=":
                if dataSL == cantidad:
                    if whoMail == "ia":
                        sendE.send(base64.b64encode(
                            cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'))
                    elif whoMail == "bk":
                        try:
                            re = requests.post(http_s + "://" + ips + ":" + port + "/" + endpoint, json={'image': base64.b64encode(
                                cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'), 'condicion': "igual", "cantidad": cantidad, "cliente": toaddr})
                            if (int(re.status_code) != 200):
                                whoMail = "ia"
                                stateMail = True
                        except(requests.exceptions.Timeout, requests.exceptions.ConnectionError, requests.exceptions.HTTPError):
                            whoMail = "ia"
                            stateMail = True
                if dataSL != cantidad:
                    stateMail = True
                else:
                    stateMail = False
            elif cond == ">":
                if whoMail == "ia":
                    sendE.send(base64.b64encode(
                        cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'))
                elif whoMail == "bk":
                    try:
                        re = requests.post(http_s + "//" + ips + ":" + port + "/" + endpoint, json={'image': base64.b64encode(
                            cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'), 'condicion': "mayor", "cantidad": cantidad, "cliente": toaddr})
                        if (int(re.status_code) != 200):
                            whoMail = "ia"
                            stateMail = True
                    except(requests.exceptions.Timeout, requests.exceptions.ConnectionError, requests.exceptions.HTTPError):
                        whoMail = "ia"
                        stateMail = True
                if dataSL < cantidad:
                    stateMail = True
                else:
                    stateMail = False


flaf_sd = 0
time_sd = 0


def sendData():
    global socketIO
    global frameS
    global dataS
    global dataSMemory
    global timeF
    global flaf_sd
    global time_sd
    global totalUp2
    global totalDown2
    global totalIngreso2
    global totalSalida2

    while(1):
        lockTime.acquire()
        timeFL = timeF
        lockTime.release()
        # print('sendData timeFL:', timeFL)
        if timeFL == 1:
            lockData.acquire()
            frameSL = deepcopy(frameS)
            dataSL = deepcopy(dataS)
            In = deepcopy(totalUp2)
            Out = deepcopy(totalDown2)
            InActual = deepcopy(totalIngreso2)
            OutActual = deepcopy(totalSalida2)
            lockData.release()
            if dataSL != dataSMemory:
                rstate = "true"
                dataSMemory = dataSL
            else:
                rstate = "false"

            if InActual > 0:
                instate = "true"
            else:
                instate = "false"

            if OutActual > 0:
                outstate = "true"
            else:
                outstate = "false"
            socketIO.emit('data', {'id': id, 'rState': rstate, 'result': dataSL, 'inState': instate, 'in': In, 'inActual': InActual, 'outState': outstate, 'out': Out, 'outActual': OutActual, 'type': 'incomep', 'frame': base64.b64encode(
                cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8')})
            lockTime.acquire()
            timeF = 0
            lockTime.release()
            flaf_sd += 1
            print('envie frame:', flaf_sd)
            temp = time.time()
            # print('tiempo envie frame', flaf_sd, ':', (temp-time_sd))
            time_sd = temp


# --------------- INICIO ARGUMENTOS ---------------
ips = env["ipStream"]
port = env["portSteram"]
x1 = int(env["x1"])
y1 = int(env["y1"])
x2 = int(env["x2"])
y2 = int(env["y2"])
id = str(env["id"])
toaddr = str(env["toaddr"])
cond = str(env["cond"])
cantidad = str(env["cantidad"])
categoria = str(env["category"])
maxDisappeared = int(env["maxDisappeared"])
maxDistance = int(env["maxDistance"])
totalFrames = int(env["totalFrames"])
passFRames = int(env["passFRames"])
detPresTresh = float(env["detPresTresh"])  # 6.0
nmsTresh = float(env["nmsTresh"])  # 0.75
drawBB = str(env["drawBB"])
modelName = str(env["modelName"])
fromaddr = str(env["fromaddr"])
passw = str(env["passw"])
whoMail = str(env["whoMail"])
endpoint = str(env["endpoint"])
xf1 = int(env["xf1"])
yf1 = int(env["yf1"])
xf2 = int(env["xf2"])
yf2 = int(env["yf2"])
frontera = [xf1, yf1, xf2, yf2]
toleracia = int(env["toleracia"])
dirIngreso = str(env["dirIngreso"])
ipsMail = env["ipsMail"]
portMail = env["portMail"]
production = env["production"]
http_s = env["http_s"]

if cantidad != 'none':
    cantidad = int(cantidad)

cat = []
if categoria == "person":

    cat.append(1)  # person
elif categoria == "bicycle":

    cat.append(2)  # cuchillo
elif categoria == "bottle":

    cat.append(44)  # botella
elif categoria == "car":

    cat.append(3)  # carro
elif categoria == "motorcycle":

    cat.append(4)  # perro

# print("category")
# --------------- FINAL ARGUMENTOS ---------------
# --------------- INICIO VARIABLES ---------------
trackers = []
trackableObjects = {}
totalDown = 0
totalUp = 0
# frontera = [11, 17, 500, 350]  # H: [0, 150, 640, 150] V: [200, 1, 200, 350]
if frontera[0] == frontera[2]:
    frontera[0] += 1
if frontera[1] == frontera[3]:
    frontera[1] += 1
# toleracia = 8
# dirIngreso = "L"
W = None
H = None
trackers = []
nombre = modelName  # "ssd_mobilenet_v1_coco_11_06_2017"
barrera = (x1, y1, x2, y2)
trama = []
frameS = []
dataS = 0
totalUp2 = 0
totalDown2 = 0
totalIngreso2 = 0
totalSalida2 = 0
dataSMemory = -1

timeF = 0
emailF = False
flag_pcss = False
flag_process = 0
time_ps = 0
stateMail = True
mitrama = []
# --------------- FINAL VARIABLES ---------------
# --------------- INICIO DEFINICION DE OBJETOS ---------------
deteccion = det(nombre, cat, detPresTresh)
sendE = sendEmail(toaddr, fromaddr, passw, cat[0], cond, cantidad),
socketIO = SocketIO(http_s + "://" + ips, port, LoggingNamespace)
ct = CentroidTracker(maxDisappeared, maxDistance)
if production == "false":
    cap = cv2.VideoCapture(0)
# --------------- FINAL DEFINICION DE OBJETOS ---------------

# --------------- INICIO DEFINICION THREADS ---------------
threadLock = threading.Lock()
lockData = threading.Lock()
lockTime = threading.Lock()
getF = threading.Lock()

hilo1 = threading.Thread(target=escuchar)
hilo2 = threading.Thread(target=sendData)
if cond != 'none':
    hilo3 = threading.Thread(target=sendEm)
    hilo3.start()
# --------------- INICIO DEFINICION THREADS ---------------
if production == "true":
    hilo1.start()
hilo2.start()


with deteccion.detection_graph.as_default():
    with tf.Session(graph=deteccion.detection_graph) as sess:
        object_tracker = []
        bandera = 0
        total_de_carros = 0
        while (1):
            if production == "true":
                tcopy = time.time()
                threadLock.acquire()
                mitrama = deepcopy(trama)
                threadLock.release()
                getF.acquire()
                flag_pcss_local = flag_pcss
                getF.release()
                tcopy2 = time.time()
            else:
                mitrama.append(0)
                flag_pcss_local = True
            if len(mitrama) > 0 and flag_pcss_local == True:
                if production == "true":
                    tgimg = time.time()
                    barray = base64.b64decode(mitrama[0])
                    jpg_as_np = np.frombuffer(barray, dtype=np.uint8)
                    image = cv2.imdecode(jpg_as_np, flags=1)
                    # if W is None or H is None:
                    #     (H, W) = image.shape[:2]
                else:
                    ret, image = cap.read()
                    image = cv2.resize(image, (int('640'), int('360')))
                if W is None or H is None:
                    (H, W) = image.shape[:2]
                if totalFrames % passFRames == 0 or total_de_carros == 0:
                    status = "Detecting"
                    ta = time.time()
                    trackers = []
                    pop2, clone, clone2 = deteccion.detector(
                        image, barrera, sess)
                    #
                    pop3 = utilidades.non_max_suppression_fast(np.array(pop2), 0.7)  # los BB que tengan un % de overlap > al threshold seran eliminados
                    # pop3 = pop2
                    # print('pop3 len:', len(pop3))

                    for BB in pop3:  # recorre cada uno de las coordenadas BBs encontradas por el detector
                        w = BB[2] - BB[0]  # se calcula el ancho del BB
                        h = BB[3] - BB[1]  # se calcula el alto del BB
                        # BB_inicializacion = (BB[0], BB[1], w, h)
                        # tracker = cv2.TrackerCSRT_create()
                        # tracker.init(image, BB_inicializacion)

                        tracker = dlib.correlation_tracker()
                        rect = dlib.rectangle(BB[0], BB[1], BB[2], BB[3])
                        tracker.start_track(image, rect)

                        # add the tracker to our list of trackers so we can
                        # utilize it during skip frames
                        trackers.append(tracker)
                        # se dibuja de color azul sobre clone2 el BB
                        if drawBB == 'true':
                            cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (0, 255, 0), 4)
                    tb = time.time()
                    # print('deteccion:', tb - ta)
                else:
                    tc = time.time()
                    pop3 = []
                    clone2 = image.copy()
                    i = 0
                    delTacker = []
                    for tracker in trackers:
                        status = "Tracking"
                        tracker.update(image)
                        pos = tracker.get_position()
                        startX = int(pos.left())
                        startY = int(pos.top())
                        endX = int(pos.right())
                        endY = int(pos.bottom())
                        centX = int(startX + ((endX - startX) / 2))
                        centY = int(startY + ((endY - startY) / 2))
                        if centX > barrera[0] and centX < barrera[2] and centY > barrera[1] and centY < barrera[3]:
                            # add the bounding box coordinates to the rectangles list
                            pop3.append((startX, startY, endX, endY))
                            cv2.rectangle(clone2, (startX, startY), (endX, endY), (255, 0, 0), 4)
                        else:
                            delTacker.append(i)
                        i += 1
                    for i in range((len(delTacker) - 1), -1, -1):
                        index = delTacker[i]
                        del trackers[index]
                    cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)
                    td = time.time()
                    # print('tracking:', td - tc)
                totalFrames += 1
                cv2.line(clone2, (frontera[0], frontera[1]), (frontera[2], frontera[3]), (0, 255, 255), 2)
                objects = ct.update(pop3)
                totalIngreso = 0
                totalSalida = 0
                for (objectID, centroid) in objects.items():
                    to = trackableObjects.get(objectID, None)

                    # if there is no existing trackable object, create one
                    if to is None:
                        to = TrackableObject(objectID, centroid, frontera, toleracia, dirIngreso)

                    # otherwise, there is a trackable object so we can utilize it
                    # to determine direction
                    else:
                        to.centroids.append(centroid)

                        ingreso, salida = to.changeSide()
                        totalUp += ingreso
                        totalDown += salida

                        totalIngreso += ingreso
                        totalSalida += salida
                        # print(to.centroids)
                        # # print('direction', direction)
                        # repos = centroid[1] < H // 2
                        # # print('repos', repos)
                        #
                        # # check to see if the object has been counted or not
                        # if not to.counted:
                        #     # if the direction is negative (indicating the object
                        #     # is moving up) AND the centroid is above the center
                        #     # line, count the object
                        #     if direction < 0 and centroid[1] < H // 2:
                        #         totalUp += 1
                        #         to.counted = True
                        #
                        #     # if the direction is positive (indicating the object
                        #     # is moving down) AND the centroid is below the
                        #     # center line, count the object
                        #     elif direction > 0 and centroid[1] > H // 2:
                        #         totalDown += 1
                        #         to.counted = True

                    # store the trackable object in our dictionary
                    trackableObjects[objectID] = to

                    # draw both the ID of the object and the centroid of the
                    # object on the output frame
                    # draw both the ID of the object and the centroid of the
                    # object on the output frame
                    text = "ID {}".format(objectID)
                    # # print(text)
                    cv2.putText(clone2, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    cv2.circle(clone2, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                total_de_carros = len(objects.items())
                print('total_de_carros:', total_de_carros, 'len(pop3)', len(pop3))
                if production == "false":
                    flag_process += 1
                    print('frame:', flag_process)
                    info = [
                        ("Ingreso Der a Iz", totalUp),
                        ("Salida Iz a Der", totalDown),
                        ("status", status),
                        ("frame", flag_process),
                    ]
                    # # print(status)
                    for (i, (k, v)) in enumerate(info):
                        text = "{}: {}".format(k, v)
                        cv2.putText(clone2, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)
                    # print(total_de_carros)
                    # posicion_print_numCarTotal = (5, 50)
                    # cv2.putText(clone2, "cantidad: " + str(total_de_carros), posicion_print_numCarTotal, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
                    cv2.namedWindow('Output', cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('Output', 640, 360)
                    cv2.imshow('Output', clone2)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                lockTime.acquire()
                timeF = 1
                lockTime.release()
                if production == "true":
                    getF.acquire()
                    flag_pcss = False
                    getF.release()

                lockData.acquire()
                frameS = deepcopy(clone2)
                dataS = total_de_carros
                totalUp2 = totalUp
                totalDown2 = totalDown
                totalIngreso2 = totalIngreso
                totalSalida2 = totalSalida
                lockData.release()
