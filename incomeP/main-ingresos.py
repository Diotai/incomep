# from socketIO_client_nexus import SocketIO, LoggingNamespace
from libs.deteccion import deteccion as det
from libs.openVideo import openVideo
from libs.centroidtracker import CentroidTracker
from libs.trackableobject import TrackableObject
from libs.sendEmail import sendEmail
from imutils.video import FPS
from copy import deepcopy
from os import environ as env
import tensorflow as tf
import numpy as np
import utilidades
import threading
# import argparse
import base64
import time
import cv2
import dlib

flag_gT = 0
time_gt = 0


def getFrame(*args):
    global trama
    global flag_gT
    global time_gt
    # global flag_pcss
    threadLock.acquire()
    trama = deepcopy(args)
    threadLock.release()
    # getF.acquire()
    # flag_pcss = True
    # getF.release()
    flag_gT += 1
    # print("recibi frame:", flag_gT)
    temp = time.time()
    # print('tiempo recibi frame:', (temp-time_gt))
    time_gt = temp


def escuchar():
    global socketIO

    while(1):
        socketIO.on('frame', getFrame)
        socketIO.wait(seconds=0.001)


def sendEm():
    global socketIO
    global frameS
    global dataS
    global timeF
    global emailF
    global cond
    while(1):
        lockTime.acquire()
        timeFL = timeF
        lockTime.release()
        if timeFL == 1:
            lockData.acquire()
            frameSL = deepcopy(frameS)
            dataSL = deepcopy(dataS)
            lockData.release()

            if emailF is False:
                if cond == "=":
                    if dataSL == cantidad:
                        emailF = True
                        sendE.send(base64.b64encode(
                            cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'))
                elif cond == ">":
                    emailF = True
                    if dataSL > cantidad:
                        sendE.send(base64.b64encode(
                            cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'))
                else:
                    emailF = False
            lockTime.acquire()
            timeF = 0
            lockTime.release()


flaf_sd = 0
time_sd = 0


def sendData():
    global socketIO
    global frameS
    global dataS
    global timeF
    global flaf_sd
    global time_sd

    while(1):
        lockTime.acquire()
        timeFL = timeF
        lockTime.release()
        if timeFL == 1:
            lockData.acquire()
            frameSL = deepcopy(frameS)
            dataSL = deepcopy(dataS)
            lockData.release()
            socketIO.emit('data', {'id': id, 'result': dataSL, 'frame': base64.b64encode(
                cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8')})
            lockTime.acquire()
            timeF = 0
            lockTime.release()
            flaf_sd += 1
            print('envie frame:', flaf_sd)
            temp = time.time()
            # print('tiempo envie frame', flaf_sd, ':', (temp-time_sd))
            time_sd = temp


# --------------- INICIO ARGUMENTOS ---------------
# ap = argparse.ArgumentParser()
# ap.add_argument("-ipStream", "--ipStream", required=True, help="server")
# ap.add_argument("-portSteram", "--portSteram", required=True, help="server")
# ap.add_argument("-x1", "--x1", required=True, help="x1")
# ap.add_argument("-y1", "--y1", required=True, help="y1")
# ap.add_argument("-x2", "--x2", required=True, help="x2")
# ap.add_argument("-y2", "--y2", required=True, help="y2")
# ap.add_argument("-category", "--category", required=True, help="c1")
# ap.add_argument("-id", "--id", required=True, help="id app")
# ap.add_argument("-toaddr", "--toaddr", required=True, help="toaddr")
# ap.add_argument("-cond", "--cond", required=True, help="cond alert")
# ap.add_argument("-cantidad", "--cantidad", required=True, help="cantidad alert")

# args = vars(ap.parse_args())
ips = env["ipStream"]
port = env["portSteram"]
x1 = int(env["x1"])
y1 = int(env["y1"])
x2 = int(env["x2"])
y2 = int(env["y2"])
id = str(env["id"])
toaddr = str(env["toaddr"])
cond = str(env["cond"])
cantidad = str(env["cantidad"])
categoria = str(env["category"])
maxDisappeared = int(env["maxDisappeared"])
maxDistance = int(env["maxDistance"])

if cantidad != 'none':
    cantidad = int(cantidad)

cat = []
if categoria == "person":
    pCate = "Persona"
    cat.append(1)  # person 1

elif categoria == "bicycle":
    pCate = "Bicicleta"
    cat.append(2)  # cuchillo

elif categoria == "bottle":
    pCate = "Botella"
    cat.append(44)  # botella

elif categoria == "car":
    pCate = "Vehiculo"
    cat.append(3)  # carro

elif categoria == "motorcycle":
    pCate = "Motocicleta"
    cat.append(4)  # perro

# print("category")
# --------------- FINAL ARGUMENTOS ---------------
# --------------- INICIO VARIABLES ---------------
trackers = []
trackableObjects = {}
totalDown = 0
totalUp = 0
frontera = [152, 9, 153, 174]  # H: [0, 150, 640, 150] V: [200, 1, 200, 350]
if frontera[0] == frontera[2]:
    frontera[0] += 1
if frontera[1] == frontera[3]:
    frontera[1] += 1
toleracia = 8
dirIngreso = "R"
W = None
H = None
nombre = "ssd_mobilenet_v1_coco_11_06_2017"
barrera = (x1, y1, x2, y2)
trama = []
frameS = []
dataS = 0
timeF = 0
emailF = False
flag_pcss = False
flag_process = 0
time_ps = 0
# --------------- FINAL VARIABLES ---------------
# --------------- INICIO DEFINICION DE OBJETOS ---------------
deteccion = det(nombre, cat, 0.7)
# sendE = sendEmail(toaddr, fromaddr, passw, cat[0], cond, cantidad)
# socketIO = SocketIO(ips, port, LoggingNamespace)
imgStreaming = openVideo("webcam", ips, port, "user", "password")
ct = CentroidTracker(maxDisappeared, maxDistance)
# --------------- FINAL DEFINICION DE OBJETOS ---------------

# --------------- INICIO DEFINICION THREADS ---------------
threadLock = threading.Lock()
lockData = threading.Lock()
lockTime = threading.Lock()
getF = threading.Lock()
hilo1 = threading.Thread(target=escuchar)
hilo2 = threading.Thread(target=sendData)
# if cond != 'none':
#     hilo3 = threading.Thread(target=sendEm)
#     hilo3.start()
# --------------- INICIO DEFINICION THREADS ---------------

# hilo1.start()
# hilo2.start()


with deteccion.detection_graph.as_default():
    with tf.Session(graph=deteccion.detection_graph) as sess:
        object_tracker = []
        bandera = 0
        fps = FPS().start()
        totalFrames = 0
        total_de_carros = 0
        while (1):
            image, pl = imgStreaming.openV()
            if W is None or H is None:
                (H, W) = image.shape[:2]
            # print(np.shape(image))
            status = "Waiting"
            if totalFrames % 15 == 0 or total_de_carros == 0 or len(trackers) != total_de_carros:
                status = "Detecting"
                ta = time.time()
                trackers = []
                pop2, clone, clone2 = deteccion.detector(
                    image, barrera, sess)
                #
                pop3 = utilidades.non_max_suppression_fast(np.array(pop2), 0.7)  # los BB que tengan un % de overlap > al threshold seran eliminados
                # pop3 = pop2
                # print('pop3:', pop3, 'len:', len(pop3))

                for BB in pop3:  # recorre cada uno de las coordenadas BBs encontradas por el detector
                    w = BB[2] - BB[0]  # se calcula el ancho del BB
                    h = BB[3] - BB[1]  # se calcula el alto del BB
                    # BB_inicializacion = (BB[0], BB[1], w, h)
                    # tracker = cv2.TrackerCSRT_create()
                    # tracker.init(image, BB_inicializacion)

                    tracker = dlib.correlation_tracker()
                    rect = dlib.rectangle(BB[0], BB[1], BB[2], BB[3])
                    tracker.start_track(image, rect)

                    # add the tracker to our list of trackers so we can
                    # utilize it during skip frames
                    trackers.append(tracker)
                    # se dibuja de color azul sobre clone2 el BB
                    cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (0, 255, 0), 4)
                tb = time.time()
                # print('deteccion:', tb - ta)
            else:
                # print('tracking')
                tc = time.time()
                pop3 = []
                clone2 = image.copy()
                i = 0
                delTacker = []
                # print(len(trackers))
                for tracker in trackers:
                    status = "Tracking"
                    tracker.update(image)
                    pos = tracker.get_position()
                    # print('pos:', pos)

                    # unpack the position object
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())
                    centX = int(startX + ((endX - startX) / 2))
                    centY = int(startY + ((endY - startY) / 2))
                    if centX > barrera[0] and centX < barrera[2] and centY > barrera[1] and centY < barrera[3]:
                        # add the bounding box coordinates to the rectangles list
                        pop3.append((startX, startY, endX, endY))
                        cv2.rectangle(clone2, (startX, startY), (endX, endY), (255, 0, 0), 4)
                    else:
                        delTacker.append(i)
                    i += 1
                for i in range((len(delTacker) - 1), -1, -1):
                    index = delTacker[i]
                    del trackers[index]
                cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)
                td = time.time()
                # print('tracking:', td - tc)
            totalFrames += 1
            cv2.line(clone2, (frontera[0], frontera[1]), (frontera[2], frontera[3]), (0, 255, 255), 2)
            objects = ct.update(pop3)
            for (objectID, centroid) in objects.items():
                to = trackableObjects.get(objectID, None)

                # if there is no existing trackable object, create one
                if to is None:
                    to = TrackableObject(objectID, centroid, frontera, toleracia, dirIngreso)
                else:
                    to.centroids.append(centroid)

                    ingreso, salida = to.changeSide()
                    totalUp += ingreso
                    totalDown += salida

                trackableObjects[objectID] = to

                text = "ID {}".format(objectID)
                # print(text)
                cv2.putText(clone2, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.circle(clone2, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
            info = [
                ("Ingreso Der a Iz", totalUp),
                ("Salida Iz a Der", totalDown),
                ("status", status),
            ]
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(clone2, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)
            total_de_carros = len(objects.items())
            # print(total_de_carros)
            posicion_print_numCarTotal = (5, 50)
            cv2.putText(clone2, "cantidad: " + str(total_de_carros), posicion_print_numCarTotal, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
            cv2.namedWindow('Output', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Output', 640, 360)
            cv2.imshow('Output', clone2)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            fps.update()
            # t2 = time.time()
            # lockTime.acquire()
            # timeF = 1
            # lockTime.release()
            #
            # lockData.acquire()
            # frameS = deepcopy(clone2)
            # dataS = total_de_carros
            # lockData.release()
            # # getF.acquire()
            # # flag_pcss = False
            # # getF.release()
            # flag_process += 1
            # # print('procese frame:', flag_process)
            # # print('time decode image', tgimg2 - tgimg)
            # # print('tiempo procees frame:', t2 - t1)
            # # print(total_de_carros)
            # temp = time.time()
            # # print('tiempo procese', flag_process, ':', (temp-time_ps))
            # time_ps = temp
        fps.stop()
        print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
