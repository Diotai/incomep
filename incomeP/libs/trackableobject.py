class TrackableObject:
    def __init__(self, objectID, centroid, frontera, toleracia, dirIngreso):
        # store the object ID, then initialize a list of centroids
        # using the current centroid
        self.objectID = objectID
        self.centroidComparacion = centroid
        self.centroids = [centroid]
        self.frontera = frontera
        self.x1 = frontera[0]
        self.y1 = frontera[1]
        self.x2 = frontera[2]
        self.y2 = frontera[3]
        self.toleracia = toleracia
        self.dirIngreso = dirIngreso
        self.m = (self.y2 - self.y1) / (self.x2 - self.x1)
        self.sideCentroidComparacion = self.posYSide(self.centroidComparacion)

        # initialize a boolean used to indicate if the object has
        # already been counted or not
        self.counted = False

    def posYSide(self, centroide):
        x = centroide[0]
        y = centroide[1]
        yf = (self.m * (x - self.x1)) + self.y1
        side = y < yf  # True: L/U ; False: R/D
        # print('y:', y, 'yf:', yf, 'm:', self.m)
        return side

    def changeSide(self):
        L = len(self.centroids) - 1
        numtole = 0
        ingreso = 0
        salida = 0
        # print('-------------------------------')
        for i in range(L, L - self.toleracia, -1):
            if i < 0:
                continue
            side = self.posYSide(self.centroids[i])
            # print('i:', i, 'centroids:', self.centroids[i], 'side:', side, 'self.sideCentroidComparacion:', self.sideCentroidComparacion)
            if self.sideCentroidComparacion != side:
                # print('desigual', 'i:', i)
                numtole += 1
        if numtole == self.toleracia:
            # print('entreeeee')
            if self.dirIngreso == "L" or self.dirIngreso == "U":
                # print('sideCentroidComparacion - before - 1', self.sideCentroidComparacion, 'self.centroidComparacion - before - 1', self.centroidComparacion)
                if self.sideCentroidComparacion == False:
                    # print('entre1')
                    ingreso = 1
                    salida = 0
                else:
                    # print('entre2')
                    # print('sideCentroidComparacion - before', self.sideCentroidComparacion, 'self.centroidComparacion - before', self.centroidComparacion)
                    ingreso = 0
                    salida = 1
            if self.dirIngreso == "R" or self.dirIngreso == "D":
                if self.sideCentroidComparacion == True:
                    # print('entre3')
                    ingreso = 1
                    salida = 0
                else:
                    # print('entre4')
                    ingreso = 0
                    salida = 1
            self.centroidComparacion = self.centroids[L]
            self.sideCentroidComparacion = self.posYSide(self.centroidComparacion)
            # print('sideCentroidComparacion - AFTER', self.sideCentroidComparacion, 'self.centroidComparacion - AFTER', self.centroidComparacion)
        # print(self.dirIngreso)
        # print('ingreso:', ingreso, 'salida:', salida)
        return ingreso, salida
