FROM manuelfgr/tensorflow-dlib:v1
MAINTAINER Manuel Felipe Garcia Rincon "manuel.garcia@globai.co"
RUN apt update
RUN apt install cmake -y
ADD incomeP /home/incomeP
RUN pip install -r /home/incomeP/requirements.txt
WORKDIR /home/incomeP/
CMD ["python", "main.py"]
