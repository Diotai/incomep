import tensorflow as tf
import sys
import os
import cv2
import numpy as np
from utils import label_map_util


class deteccion:
    def __init__(self, nombre, cat, detPresTresh):
        sys.path.append("..")
        self.MODEL_NAME = nombre

        # Path to frozen detection graph. This is the actual model that is used for the object detection.
        self.PATH_TO_CKPT = self.MODEL_NAME + '/frozen_inference_graph.pb'

        # List of the strings that is used to add correct label for each box.
        self.PATH_TO_LABELS = os.path.join('./data', 'mscoco_label_map.pbtxt')

        NUM_CLASSES = 90
        self.detPresTresh = detPresTresh
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.category = cat
        print("cate", self.category)
        self.label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(self.label_map, max_num_classes=NUM_CLASSES,
                                                                         use_display_name=True)

    def detector(self, image, barrera, sess):
        if type(image) == str:
            image_np = cv2.imread(image)
        else:
            image_np = image
        image_np_expanded = np.expand_dims(image_np, axis=0)
        # Definite input and output Tensors for detection_graph
        image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
        # Actual detection.

        (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections],
                                                 feed_dict={image_tensor: image_np_expanded})

        # Visualization of the results of a detection.

        bx = np.squeeze(boxes)
        W, H, D = image_np.shape
        boxes2 = bx

        boxes2[:, 0] = boxes2[:, 0] * W
        boxes2[:, 2] = boxes2[:, 2] * W

        boxes2[:, 1] = boxes2[:, 1] * H
        boxes2[:, 3] = boxes2[:, 3] * H
        boxes2 = boxes2.astype(int)
        pop2 = []

        classes2 = np.squeeze(classes).astype(np.int32)
        scores2 = np.squeeze(scores)
        clone = image_np.copy()
        clone2 = image_np.copy()

        # barrera[0] = x1
        # barrera[1] = y1
        # barrera[2] = x2
        # barrera[3] = y2

        cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)
        for i in range(len(classes2)):
            # if classes2[i] == 44:
            if classes2[i] == self.category[0]:
                # if any(classes2[i] for classes2[i] in self.category):
                if scores2[i] >= self.detPresTresh:
                    # print(scores2[i])

                    centX = (int(boxes2[i, 1]) + int(float(boxes2[i, 3] - boxes2[i, 1]) / float(2)))
                    centY = (int(boxes2[i, 0]) + int(float(boxes2[i, 2] - boxes2[i, 0]) / float(2)))
                    if centX > barrera[0] and centX < barrera[2] and centY > barrera[1] and centY < barrera[3]:
                        pop2.append(np.array((boxes2[i, 1], boxes2[i, 0], boxes2[i, 3], boxes2[i, 2])))

        return pop2, clone, clone2
