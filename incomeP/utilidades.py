import numpy as np
from xml.etree import ElementTree as ET


def extraccion_gth(file_gth, barrera, mascara2):
    dom = ET.parse(file_gth)

    objeto = dom.findall('object')
    gth_coor = np.array([[]])
    gth2 = np.array([[]])

    if len(objeto) > 0:

        for coordenadas in objeto:
            if int(coordenadas[1].text) == 0:

                if int(coordenadas[9][3][0].text) < int(coordenadas[9][1][0].text):
                    Xstart = int(coordenadas[9][3][0].text)
                    Xend = int(coordenadas[9][1][0].text)
                else:
                    Xstart = int(coordenadas[9][1][0].text)
                    Xend = int(coordenadas[9][3][0].text)

                if int(coordenadas[9][3][1].text) < int(coordenadas[9][1][1].text):
                    Ystart = int(coordenadas[9][3][1].text)
                    Yend = int(coordenadas[9][1][1].text)
                else:
                    Ystart = int(coordenadas[9][1][1].text)
                    Yend = int(coordenadas[9][3][1].text)
                if (Ystart <= 479) and (Yend <= 479) and (Xstart <= 639) and (Xend <= 639):
                    if int(Xstart) + int(float(Xend - Xstart) / float(2)) > barrera:
                        if mascara2[int(Ystart), int(Xstart), 0] == 1 & mascara2[int(Yend), int(Xend), 0] == 1:
                            if gth_coor.shape[1] < 1:
                                gth_coor = np.append(
                                    gth_coor, [(Xstart, Ystart, Xend, Yend)], axis=1)
                            else:
                                gth_coor = np.append(
                                    gth_coor, [(Xstart, Ystart, Xend, Yend)], axis=0)
                        else:
                            if gth2.shape[1] < 1:
                                gth2 = np.append(gth2, [(Xstart, Ystart, Xend, Yend)], axis=1)
                            else:
                                gth2 = np.append(gth2, [(Xstart, Ystart, Xend, Yend)], axis=0)
                    else:
                        if gth2.shape[1] < 1:
                            gth2 = np.append(gth2, [(Xstart, Ystart, Xend, Yend)], axis=1)
                        else:
                            gth2 = np.append(gth2, [(Xstart, Ystart, Xend, Yend)], axis=0)
        return gth_coor, gth2
    return gth_coor, gth2


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    if (boxB[0] < boxA[2]) & (boxB[2] > boxA[0]) & (boxB[1] < boxA[3]) & (boxB[3] > boxA[1]):
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])

        # compute the area of intersection rectangle
        interArea = (xB - xA) * (yB - yA)

        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[2] - boxA[0]) * (boxA[3] - boxA[1])
        boxBArea = (boxB[2] - boxB[0]) * (boxB[3] - boxB[1])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)
        # if iou <= 0:
        # iou = 0
    else:
        iou = 0
    # return the intersection over union value
    return iou


def non_max_suppression_fast(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    # print(area)
    idxs = np.argsort(y2)


    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int")
